package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.CityDao;
import fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResponse;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase.databaseWriteExecutor;

public class WeatherRepository {

    private final String url = "https://api.openweathermap.org";
    private final String apiKey = "a995085a0ef6ae1e47a29b93eaf47762";
    private final OWMInterface weatherAPI;
    private final LiveData<List<City>> allCities;
    private final MutableLiveData<City> selectedCity;
    private final CityDao cityDao;
    public MutableLiveData<Boolean> isLoading = new MutableLiveData<Boolean>();
    public MutableLiveData<Throwable> webServiceThrowable = new MutableLiveData<Throwable>();
    private static final String TAG = WeatherRepository.class.getSimpleName();
    String _city;
    String _country;
    private volatile int nbAPIloads;


    private static volatile WeatherRepository INSTANCE;

    public synchronized static WeatherRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new WeatherRepository(application);
        }

        return INSTANCE;
    }

    public WeatherRepository(Application application) {
        nbAPIloads = 1;
        WeatherRoomDatabase db = WeatherRoomDatabase.getDatabase(application);
        weatherAPI = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
                .create(OWMInterface.class);
        cityDao = db.cityDao();
        allCities = cityDao.getAllCities();
        selectedCity = new MutableLiveData<>();
    }

    public LiveData<List<City>> getAllCities() {
        return allCities;
    }

    public MutableLiveData<City> getSelectedCity() {
        return selectedCity;
    }

    public void loadWeatherCity(City city) {
        isLoading.setValue(true);
        _city = city.getName();
        _country = city.getCountry().toLowerCase();
        weatherAPI.getForecast(_city + "," + _country, apiKey).enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                if (response.isSuccessful()) {
                    WeatherResult.transferInfo(response.body(), city);
                    updateCity(city);
                } else {
                    Log.d("Error", "Erreur Code : " + response.code());
                }
                if (--nbAPIloads <= 0)
                    isLoading.postValue(false);
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable throwable) {
                webServiceThrowable.postValue(throwable);
                if (--nbAPIloads <= 0)
                    isLoading.postValue(false);
            }
        });
    }

    public void loadWeatherAllCities() throws ExecutionException, InterruptedException {
        Future<List<City>> cities = databaseWriteExecutor.submit(() -> cityDao.getSynchrAllCities());
        nbAPIloads = cities.get().size();
        try {
            for (City city : cities.get())
                loadWeatherCity(city);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public long insertCity(City newCity) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return cityDao.insert(newCity);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(newCity);
        return res;
    }

    public int updateCity(City city) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return cityDao.update(city);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(city);
        return res;
    }

    public void deleteCity(long id) {
        databaseWriteExecutor.execute(() -> {
            cityDao.deleteCity(id);
        });
    }

    public void getCity(long id)  {
        Future<City> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return cityDao.getCityById(id);
        });
        try {
            selectedCity.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
